﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AppAuth.Unidos.Migrations
{
    public partial class NewUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "2d1af19f-b6a3-4f62-b3f8-c5702ab04b6b", 0, "bfae3671-6d8b-4715-b3ee-7dfe055f2a60", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAEDmfcjbEPgEPrDtUs5K20s1msBFFpGgE93uPbsirKgCaThYf3L+iTP1jVkaydk8zvw==", null, false, "", false, "admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2d1af19f-b6a3-4f62-b3f8-c5702ab04b6b");
        }
    }
}
