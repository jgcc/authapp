﻿using AppAuth.Unidos.Database;
using AppAuth.Unidos.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AppAuth.Unidos.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<UserApp> _signInManager;
        private readonly UserManager<UserApp> _userManager;

        public AccountController(SignInManager<UserApp> signInManager, UserManager<UserApp> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public IActionResult Login(string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View(new LoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel viewModel, string? ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;

            if (!ModelState.IsValid)
            {
                viewModel.Password = string.Empty;
                return View(viewModel);
            }

            if (string.IsNullOrEmpty(ReturnUrl))
            {
                ReturnUrl = Url.Content("~/");
            }

            var usuario = await _userManager.FindByNameAsync(viewModel.Username);
            if (usuario is null)
            {
                viewModel.Password = string.Empty;
                return View(viewModel);
            }

            var resultado = await _signInManager.PasswordSignInAsync(
                usuario,
                viewModel.Password,
                isPersistent: false,
                lockoutOnFailure: false);

            if (!resultado.Succeeded) 
            {
                viewModel.Password = string.Empty;
                return View(viewModel);
            }            

            return LocalRedirect(ReturnUrl);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            if (_signInManager.IsSignedIn(User))
            {
                await _signInManager.SignOutAsync();
            }

            return RedirectToActionPermanent("Login", "Account");
        }
    }
}
