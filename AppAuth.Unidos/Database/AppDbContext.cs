﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AppAuth.Unidos.Database
{
    public sealed class AppDbContext : IdentityDbContext<UserApp>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            PasswordHasher<UserApp> passwordHasher = new PasswordHasher<UserApp>();

            var user = new UserApp
            {
                Id = "2d1af19f-b6a3-4f62-b3f8-c5702ab04b6b",
                UserName = "admin",
                NormalizedUserName = "admin".ToUpper(),
                Email = "admin@gmail.com",
                NormalizedEmail = "admin@gmail.com".ToUpper(),
                LockoutEnabled = false,
                SecurityStamp = string.Empty
            };

            user.PasswordHash = passwordHasher.HashPassword(user, "admin");
            builder.Entity<UserApp>().HasData(user);

            base.OnModelCreating(builder);
        }
    }
}
