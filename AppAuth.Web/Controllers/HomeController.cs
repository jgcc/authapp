﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using AppAuth.Web.Models;
using Microsoft.AspNetCore.Authorization;
using AppAuth.Web.ViewModels;

namespace AppAuth.Web.Controllers;

[Authorize]
public class HomeController : Controller
{
    private readonly IHttpClientFactory _httpClientFactory;

    public HomeController(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }

    public async Task<IActionResult> Index()
    {
        var httpClient = _httpClientFactory.CreateClient("default");
        var response = await httpClient.GetFromJsonAsync<IEnumerable<WeatherForecastViewModel>>("WeatherForecast");        
        return View(response);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
