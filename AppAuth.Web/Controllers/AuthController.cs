using System.Security.Claims;
using System.Text;
using System.Text.Json;
using AppAuth.Web.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace AppAuth.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public AuthController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Login(string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View(new LoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel viewModel, string? ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            if (!ModelState.IsValid)
            {
                viewModel.Password = string.Empty;
                return View(viewModel);
            }

            if(string.IsNullOrEmpty(ReturnUrl))
            {
                ReturnUrl = Url.Content("~/");
            }

            // login a la API 
            string json = JsonSerializer.Serialize(viewModel);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var httpClient = _httpClientFactory.CreateClient("default");
            var response = await httpClient.PostAsync("auth/login", content);
            // token de sesion de la api
            string token = await response.Content.ReadAsStringAsync(); 

            // generar cookie para el navegador guardando el token
            var claims = new List<Claim>
            { 
                new Claim(ClaimTypes.Name, viewModel.Username),
                new Claim("jwt_key", token),
                new Claim(ClaimTypes.Role, "admin")
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(15),
                IsPersistent = true,
                IssuedUtc = DateTimeOffset.UtcNow
            };            

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

            return LocalRedirect(ReturnUrl);
        }

        public IActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(RegisterViewModel viewModel)
        {
            if(!ModelState.IsValid)
            {
                viewModel.Password = string.Empty;
                viewModel.ValidatePassword = string.Empty;
                return View(viewModel);
            }

            return View();
        }
    }
}