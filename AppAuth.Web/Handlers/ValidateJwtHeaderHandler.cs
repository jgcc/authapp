﻿using System.Net;
using System.Net.Http.Headers;

namespace AppAuth.Web.Handlers
{
    public sealed class ValidateJwtHeaderHandler : DelegatingHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ValidateJwtHeaderHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var token = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(e => e.Type == "jwt_key");
            
            if (token == null) 
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The API key header Bearer is required.")
                };
            }

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.Value);

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
