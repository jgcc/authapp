using System.ComponentModel.DataAnnotations;

namespace AppAuth.Web.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [MinLength(5)]
        public string Username { get; set; } = string.Empty;

        [Required]
        [MinLength(4)]
        public string Password { get; set; } = string.Empty;
    }
}