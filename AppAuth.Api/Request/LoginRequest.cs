﻿using System.ComponentModel.DataAnnotations;

namespace AppAuth.Api.Request
{
    public sealed class LoginRequest
    {
        [Required]
        [MinLength(5)]
        public string Username { get; set; } = string.Empty;

        [Required]
        [MinLength(4)]
        public string Password { get; set; } = string.Empty;
    }
}
