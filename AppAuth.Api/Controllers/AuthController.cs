using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AppAuth.Api.Request;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AppAuth.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;

        public AuthController(IConfiguration config)
        {
            _config = config;
        }

        [HttpPost(nameof(Login))]
        public IActionResult Login([FromBody] LoginRequest request)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            // agregar cosas de validar si existe el usuario o no

            return Ok(GenerarToken());
        }

        [HttpPost(nameof(Register))]
        public IActionResult Register()
        {
            return Ok();
        }

        private string GenerarToken()
        {
            var key = _config.GetSection("Jwt:Key").Value;
            var issuer = _config.GetSection("Jwt:Issuer").Value;
            var audience = _config.GetSection("Jwt:Audience").Value;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, "usuario@gmail.com"),
                new Claim(ClaimTypes.Role, "Admin")
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512Signature);
            var securityToken = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddMinutes(15),
                issuer: issuer,
                audience: audience,
                signingCredentials: signingCredentials
            );
            var token = new JwtSecurityTokenHandler().WriteToken(securityToken);
            return token;
        }
    }
}